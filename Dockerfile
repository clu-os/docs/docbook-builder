FROM ubuntu:24.04

ENV DEBIAN_FRONTEND='noninteractive'
ENV LANG='en_US.UTF-8'

RUN \
	echo "${LANG} UTF-8" >> /etc/locale.gen; \
	\
	apt-get update; \
	\
	apt-get --assume-yes install --no-install-recommends \
		locales \
		xz-utils \
		perl git make xsltproc docbook-xml docbook-xsl libxml2-utils tidy \
	; \
	apt-get clean;
